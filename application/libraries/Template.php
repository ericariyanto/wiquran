<?php
class Template {

    protected $_ci;

    function __construct() {
        $this->_ci = &get_instance();
    }

    
    function display($template, $data = NULL, $js = NULL, $css = NULL) {      
        $data['_content']   = $this->_ci->load->view($template, $data, TRUE);
        $data['_header']    = $this->_ci->load->view('themes/default/header', $data, TRUE);
        $data['_menu']      = $this->_ci->load->view('themes/default/menu', $data, TRUE);
        $data['_base']      = $this->_ci->load->view('themes/default/template.php', $data);
    }









    function ajax_content($template, $data = NULL){
        $this->_ci->load->view('templates/page_header', $data);
        $this->_ci->load->view($template, $data);
    }

    //* BACKEND */
    function backend($template, $data = NULL, $js = NULL, $css = NULL) {        

        // $data['_content']       = $this->_ci->load->view($template, $data, TRUE);
        $data['_header']        = $this->_ci->load->view('backend/header', $data, TRUE);
        // $data['_slider']        = $this->_ci->load->view('backend/slider', $data, TRUE);
        // $data['_menu']          = $this->_ci->load->view('templates/menu', $data, TRUE);
        $data['_sidebar']       = $this->_ci->load->view('backend/sidebar', $data, TRUE);
        $data['_quick_sidebar'] = $this->_ci->load->view('backend/quick_sidebar', $data, TRUE);
        $data['_footer']        = $this->_ci->load->view('backend/footer', $data, TRUE);
        $data['_js_core']       = $this->_ci->load->view('backend/js_core', $data, TRUE);
        $data['_js']            = $this->_ci->load->view('backend/js', $js, TRUE);

        $data['_css']           = $this->_ci->load->view('backend/css', $css, TRUE);
        // $data['_top_bar']       = $this->_ci->load->view('templates/top_bar', $data, TRUE);
        // $data['_custom_style']  = $this->_ci->load->view('templates/custom_style', $data, TRUE);
        $data['_base']          = $this->_ci->load->view('backend/template.php', $data);
    }

    function ajax_backend($template, $data = NULL, $js = null, $css = null){        
        $data['_css']               = $this->_ci->load->view('backend/css', $css, TRUE);
        $data['_js']                = $this->_ci->load->view('backend/js', $js, TRUE);
        $data['_page_header']       = $this->_ci->load->view('backend/page_header', $data, TRUE);                
        $data['_content']           = $this->_ci->load->view($template, $data, TRUE);
        $data['_base']              = $this->_ci->load->view('backend/ajax.php', $data);
    }

}

?>
