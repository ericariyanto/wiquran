<div class="col-xs-12 col-md-3">
  <div class="nav-side-menu">
    <div class="brand">WiQuran</div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
                <li class="<?php echo (my_uri(1)=="" OR my_uri(1)=="home" ) ? 'active' : ''; ?>">
                  <a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard fa-lg"></i> Beranda</a>
                </li>

                <li  data-toggle="collapse" data-target="#surah" class="collapsed <?php echo (my_uri(2)=="surah") ? 'active' : ''; ?>">
                  <a href="#"><i class="fa fa-gift fa-lg"></i> Daftar Surah <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="surah">
                    <?php $surah = list_surah(); ?>
                    <?php $i=1; foreach ($surah as $rows) : ?>
                      <li class="<?php echo (my_uri(3) == $rows->surah_id AND my_uri(2)=="surah") ? 'active' : '' ; ?>" ><a href="<?php echo base_url('quran/surah/' . $rows->surah_id); ?>"><?php echo $i. ". " . $rows->surah_text; ?></a></li>  
                    <?php $i++; endforeach; ?>                    
                </ul>

                <li class="<?php echo (my_uri(2)=="search" ) ? 'active' : ''; ?>">
                  <a href="<?php echo base_url('quran/search'); ?>"><i class="fa fa-dashboard fa-lg"></i> Pencarian</a>
                </li>

                <li data-toggle="collapse" data-target="#life" class="collapsed">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Life Guide <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="life">
                  <li>Create New</li>
                  <li>List Data</li>                  
                </ul>            

                 <li><a href="#"><i class="fa fa-user fa-lg"></i> About</a></li>

                 <li><a href="#"><i class="fa fa-users fa-lg"></i> Users</a></li>
            </ul>
     </div>
  </div>
</div>