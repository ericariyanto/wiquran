<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo (config_app('app_title') == "") ? "WiQuran Beta Version" : config_app('app_title');?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('themes'); ?>/default/css/mystyle.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
    <script type="text/javascript">CI_ROOT = '<?php echo base_url() ?>';</script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>      
    <div class="container-fluid">
      <div class="row">
          <?php echo @$_menu; ?>
          <div class="col-xs-12 col-md-9">
            <div class="row">
                <?php echo @$_content; ?>                                 
            </div>
          </div>        
      </div>
    </div>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets'); ?>/js/jquery_blockUI.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/jquery_highlight.js"></script>
    <script src="<?php echo base_url('assets'); ?>/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url('assets/plugins'); ?>/audiojs/audio.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/myApp.js"></script>
  </body>
</html>