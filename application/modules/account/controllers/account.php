<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct() {
        parent::__construct();   
        $this->load->model('m_account'); 
    }
    
	public function index()
	{
		$this->register();
	}

	public function register(){
		$data['title'] 		= 'Account Registration';
		$data['login_url'] 	= $this->facebook->login_url();		
		$data['userfb']	   	= $this->facebook->get_user();		
		$this->template->display('register', $data);
	}

	public function register_action(){
		$input = $this->input->post();
		if(!$input) redirect('home');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

		if ($this->form_validation->run() == FALSE)
		{
			$this->register();
		}else{
			$name[] = $input['fname'];
			$name[] = $input['mname'];
			$name[] = $input['lname'];

			$data['account'] 	= array(
				'users_email' => $input['email'],
				'users_fname' => $input['fname'],
				'users_mname' => $input['mname'],
				'users_lname' => $input['lname'],
				'users_gender' => $input['gender'],
				'users_name' => implode(" ", $name),
				'users_password' => myencrypt($input['password'],TRUE)
			);

			$fb 		= $this->facebook->get_user();
			if (count($fb) > 0) {
				$data['fb'] = array(
					'users_social_type' 	=> 'fb',
					'users_social_email' 	=> $fb['email'],
					'users_social_code' 	=> $fb['id'],
					'users_social_link' 	=> $fb['link']
				);	
			}

			$save 	= $this->m_account->register($data);
			if ($save) redirect('main');
			else redirect('account/register');
		}		
		
	}

}