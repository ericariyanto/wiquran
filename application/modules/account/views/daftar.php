


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo config_app('app_title'); ?></title>

  <link href="<?= base_url(); ?>assets/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url(); ?>assets/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url(); ?>assets/font/font.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url('themes/default'); ?>/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
  <!-- BEGIN HEADER -->
  <div class="header-bg">
    <div class="container">
      <div class="row">
        <div class="header col-md-12">
          <div class="logo-nama">
            <a href="<?php echo base_url(); ?>" title="<?php echo config_app('app_desc'); ?>"><?php echo config_app('app_title'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END HEADER -->
  
  <!-- BEGIN CONTENT -->  
  <div class="box-main">
    <div class="container-fluid">
      <div class="row">
        <div class="main-container">
          <div class="tab-content">
            <a href="#" class="aktif">Tweet</a>
            <a href="#">Foto/Video</a>
            <div class="clearfix"></div>
          </div>

          <div class="content-box">
            <div class="box-content">
              <div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="true" data-auto-logout-link="false"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>  
  <!-- END CONTENT -->

  <div id="fb-root"></div>
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&appId=198527633531006&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
    }
  </script>

  <!-- JS FILE HERE -->
  <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/bootstrap/bootstrap.min.js"></script>
</body>
</html>