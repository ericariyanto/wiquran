<div class="block-container">
    <div class="col-xs-12 col-md-12 ">
      <div class="block-title">
        <div class="row">            
            <div class="col-xs-12 col-md-12 text-center surah"><?php echo @$title; ?></div>                      
        </div>                        
      </div>                      
    </div> 
    
    <div class="col-xs-12 col-md-12 ">
      <div class="block-content">
        <div class="row">
          <div class="col-md-12 col-xs-12 text-center">
          		<form action="<?php echo base_url('/account/register_action'); ?>" class="form-horizontal" id="form-cari" method="POST">
      				  <div class="form-group">
                  <label for="inputName" class="col-sm-2 col-md-2 control-label">Full Name</label>
                  <div class="col-sm-10 col-md-10">
                    <div class="row">
                      <div class="col-sm-12 col-md-4">
                        <input name="fname" value="<?php echo (set_value('fname') != "") ? set_value('fname') : @$userfb['first_name']; ?>" type="text" class="form-control col-md3" id="fname" placeholder="First Name">    
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <input name="mname" value="<?php echo @$userfb['middle_name']; ?>" type="text" class="form-control col-md3" id="mname" placeholder="Middle Name">   
                      </div>
                      <div class="col-sm-12 col-md-4">
                        <input name="lname" value="<?php echo @$userfb['last_name']; ?>" type="text" class="form-control col-md3" id="lname" placeholder="Last Name">    
                      </div>
                    </div>                                                          
                  </div>

                  <div class="col-sm-2 col-md-2 text-left"></div>
                  <div class="col-sm-10 col-md-10 text-left"><?php echo form_error('fname'); ?></div>
                </div>                

                <div class="form-group">
                  <label for="inputGender" class="col-sm-2 control-label">Gender</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-12 col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="gender" id="rdMale" value="1" checked="checked"> Male
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="gender" id="rdFemale" value="0" <?php echo (@$userfb['gender'] == 'female') ? 'checked="checked"' : ''; ?>> Female
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <input value="<?php echo (set_value('email') != "") ? set_value('email') : $userfb['email']; ?>" name="email" type="email" class="form-control" id="email" placeholder="Email">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2 col-md-2 text-left"></div>
                  <div class="col-sm-10 col-md-10 text-left"><?php echo form_error('email'); ?></div>
                </div>

                <div class="form-group">
                  <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-12 col-md-6">
                        <input value="<?php echo set_value('password'); ?>" name="password" type="password" class="form-control" id="inputPassword3" placeholder="Password">
                      </div>                                        
                    </div>                    
                  </div>
                  <div class="col-sm-2 col-md-2 text-left"></div>
                  <div class="col-sm-10 col-md-10 text-left"><?php echo form_error('password'); ?></div>
                </div>

                
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10 text-left">
                    <button type="submit" class="btn btn-primary">Register</button>
                    <a href="<?php echo @$login_url; ?>" class="btn btn-success">Using Facebook</a>
                  </div>
                </div>
      				</form>
          </div>          
        </div>                      
      </div>                    
    </div>

    <div id="result"></div>

</div> 