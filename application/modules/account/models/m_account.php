<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_account extends CI_Model {

	function __construct() {
        parent::__construct();           
    }
    
	public function register($data){
		$save 	= $this->db->insert('wiquran_users', $data['account']);
		$id 	= $this->db->insert_id();

		if (isset($data['fb']) AND count(@$data['fb']) > 0) {
			$data['fb']['users_id'] 	= $id;			
			$fb 	= $this->db->insert('wiquran_users_social', $data['fb']);
		}

		return $save;
	}
}