<!DOCTYPE html>
<html>
<head>
	<title><?php echo $this->config->item('app_title'); ?></title>
	<style type="text/css">
		body {
			margin: 0px;
			padding: 10px;	
		}

		.container{
			width: 94%;
			margin: 0px;
			padding: 10px 3%;
			border: 1px solid #000;
		}

		.table {
			width: 100%;
			border-collapse: collapse;				
		}	
		
		.table, .table th, .table td {
			border:1px solid #000;		
			padding: 5px;
		}	

		a, a:hover, a:visited {
			display: block;
			text-decoration: none;
			color: #000;
		}

		a:hover {
			font-weight: bold;
		}

		.quran-text {
			font-weight: bold;
			font-size: 36px;
		}
	</style>
</head>
<body>
	<div class="container">
		<table class="table">
			<thead>
				<th width="20px">No</th>
				<th align="left">Nama Surah</th>
				<th width="30px">Jumlah Ayat</th>
			</thead>
			<tbody>
				<?php foreach($surah as $list) : ?>
				<tr>
					<td align="center"><?php echo $list->surah_id; ?></td>
					<td><a href="<?php echo base_url('quran/surah/' . $list->surah_id); ?>" title="Lihat ayat pada surah <?php echo $list->surah_text; ?>"><?php echo $list->surah_text; ?></a></td>
					<td align="center"><?php echo $list->surah_jml_ayat; ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</body>
</html>