<div class="block-container">
    <div class="col-xs-12 col-md-12 ">
      <div class="block-title">
        <div class="row">            
            <div class="col-xs-12 col-md-12 text-center surah">Pencarian</div>                      
        </div>                        
      </div>                      
    </div> 
    
    <div class="col-xs-12 col-md-12 ">
      <div class="block-content">
        <div class="row">
          <div class="col-md-12 col-xs-12 text-center ayat">
          		<form class="form-inline form-cari" id="form-cari" method="POST">
				  <div class="form-group">				    
				    <input type="text" class="form-control" name="cari" id="cari" placeholder="Tulis yang ingin dicari disini..">
				  </div>				  
				  <button type="submit" name="pencarian" value="1" class="btn btn-default ">Cari</button>
				</form>
          </div>          
        </div>                      
      </div>                    
    </div>

    <div id="result"></div>

</div> 

<script type="text/javascript">
$(document).ready(function(){	
	$("#form-cari").submit(function(){		
		 // show that something is loading
		 $.blockUI({ 
		 	message: 'Silahkan tunggu..',
		 	css: { 
	            border: 'none', 
	            padding: '15px', 
	            backgroundColor: '#fff', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .5, 
	            color: '#000' 	            
	       	} 
	     });         

         $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>quran/search', 
            data: $(this).serialize()
        })
        .done(function(data){
            $.unblockUI();
            //highlight 
            var src_str = data;
			var term = $("#cari").val();			
            // show the response
            $('#result').html(src_str);            
            $("#result").highlight(term);
             
        })
        .fail(function() {
         
            // just in case posting your form failed
            alert( "Pencarian tidak ditemukan" );
            $.unblockUI();
            $('#result').html("");
             
        });

         // to prevent refreshing the whole page page
        return false;

	});
});
</script>