<div class="block-container">
    <div class="col-xs-12 col-md-12 ">
      <div class="block-title">
        <div class="row">
            <div class="col-xs-12 col-md-3 text-center surah"><?php echo $surah[0]->surah .". " . $surah[0]->surah_text; ?></div>
            <div class="col-xs-12 col-md-9 text-center bismillah"><?php show_bismillah(); ?></div>                      
        </div>                        
      </div>                      
    </div> 

    <?php foreach($surah as $list) : ?>
    <div class="col-xs-12 col-md-12 ">
      <div class="block-content">
        <div class="row">
          <div class="col-md-6 col-md-push-6 col-xs-12 text-right ayat">
            <?php echo $list->text; ?>   
          </div>  
          <div class="col-md-6 col-md-pull-6 col-xs-12 terjemahan">
            <?php echo $list->ayat . ". " . $list->trans; ?>
          </div>  

          <div class="col-md-12 col-xs-12 pull-right">
            <div class="block-tools"> 
              <a href="javascript:;"><i class="fa fa-facebook-square fa-2x"></i></a>
              <a href="javascript:;"><i class="fa fa-twitter-square fa-2x"></i></a>
              <a href="javascript:;"><i class="fa fa-google-plus-square fa-2x"></i></a>              
              <a href="javascript:;"><i class="fa fa-times-circle-o fa-2x"></i></a>
              <a href="javascript:;"><i class="fa fa-heart-o fa-2x"></i></a>
              <a href="javascript:;"><i class="fa fa-book fa-2x"></i></a>                          
            </div>
          </div>  
        </div>                      
      </div>                    
    </div>
    <?php endforeach; ?>                    
</div> 