<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quran extends CI_Controller {

	function __construct() {
        parent::__construct();   
        $this->load->model('m_quran'); 
    }
    
	public function index()
	{
		$this->surah();
	}

	public function surah($surah_id = ""){
		$surah_id 	= intval($surah_id);

		if ($surah_id == "" OR ($surah_id > '114' AND $surah_id < '1')) {
			// tampilkan list surah
			$data['surah'] 	= $this->m_quran->get_surah();
			$this->load->view('quran', $data);	
		}else{
			// tampilkan ayat pada surah
			$where 			= array('qt.surah' => $surah_id, 'lang' => 'ID');
			$limit 			= null; //10;
			$data['surah'] 	= $this->m_quran->get_surah_detail($where, $limit);
			$this->template->display('quran_surah', $data);	
		}		
	}

	public function search(){
		$input 	= $this->input->post();

		if (!$input) {
			$data 	= array();
			$this->template->display('quran_search', $data);
		}else{
			// tampilkan ayat pada surah
			$where 			= array('t.text LIKE ' => '%' . $this->db->escape_like_str($input['cari']). '%', 'lang' => 'ID');
			$limit 			= null; //10;
			$data['surah'] 	= $this->m_quran->get_surah_detail($where, $limit);
			
			$this->load->view('quran_search_result', $data);
		}
	}
}
