<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_quran extends CI_Model {

	function __construct() {
        parent::__construct();           
    }
    
	public function get_surah(){
		$this->db->select('*')->from('quran_surah');

		$sql 	= $this->db->get();
		$result = $sql->result();

		return $result;
	}

	public function get_surah_detail($where = NULL, $limit = NULL, $offset = "0"){
		$this->db->select('qt.*, s.surah_text, t.text as trans')->from('quran_text qt');
		$this->db->join('quran_surah s', 'qt.surah = s.surah_id');
		$this->db->join('quran_trans t', 'qt.surah = t.surah AND qt.ayat = t.ayat');


		if (!is_null($where)) {
			$this->db->where(array('t.status' => '1'));
			$this->db->where($where);
		}

		$this->db->order_by('qt.surah', 'asc');
		$this->db->order_by('qt.ayat', 'asc');

		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}

		$sql 	= $this->db->get();
		$result = $sql->result();

		return $result;
	}


}
