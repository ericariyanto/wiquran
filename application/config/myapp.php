<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['app_title'] 			= "Rumah Quran";
$config['app_desc'] 			= "Membaca, memahami, bertanya, diskusi segala persoalan dengan landasan Al-Quran.";
$config['app_version_major'] 	= "1";
$config['app_version_minor'] 	= "0.0.0";
$config['app_secret'] 			= md5('@ericariyanto');

// SOCIAL PLUGINS CONFIG
$config['facebook']['api_id'] 		= '198527633531006';
$config['facebook']['app_secret'] 	= '34ec219f7185ab5ad2398869c6ec0a63';
$config['facebook']['redirect_url'] = 'http://localhost:4/myproject/RQ/';
$config['facebook']['permissions'] 	= array(
										'email', 
										// 'user_location', 
										'user_birthday'
									  );