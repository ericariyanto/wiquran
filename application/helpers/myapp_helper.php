<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_murotal_code'))
{
	function get_murotal_code($surah, $ayat){	
		$surah 	= substr("000" . $surah, -3);
		$ayat 	= substr("000" . $ayat, -3);
		$url 	= $surah . $ayat;
		return $url;
	}
}

// if ( ! function_exists('get_murotal'))
// {
// 	function get_murotal($code, $by = 'Musyari'){	
// 		$url 	= base_url('sound/' . $by . '/' . $code . '.mp3');
// 		return $url;
// 	}
// }

if ( ! function_exists('myencrypt'))
{
	function myencrypt($str, $hashed = FALSE){
		$CI 	=& get_instance();
		$CI->load->library('encrypt');

		$chiper 	= $CI->encrypt->encode($str);
		$chiper 	= ($hashed) ? sha1($chiper) : $chiper;	

		return $chiper;
	}
}

if ( ! function_exists('show_bismillah'))
{
	function show_bismillah()
	{
		echo "بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ";
	}
}

if ( ! function_exists('my_uri'))
{
	function my_uri($segment = 1){
		$CI 	=& get_instance();

		return $CI->uri->segment($segment);
	}
}

if ( ! function_exists('list_surah'))
{
	function list_surah(){
		$CI 	=& get_instance();

		$CI->db->select('*')->from('quran_surah');
		$sql 	= $CI->db->get();

		return $sql->result();
	}
}

if ( ! function_exists('config_app'))
{
	function config_app($param = "app_title"){
		$CI 	=& get_instance();

		return $CI->config->item($param);
	}
}

if ( ! function_exists('cleanURL'))
{
	function cleanURL($string)
	{
	    $url = str_replace("'", '', $string);
	    $url = str_replace('%20', ' ', $url);
	    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); // substitutes anything but letters, numbers and '_' with separator
	    $url = trim($url, "-");
	    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);  // you may opt for your own custom character map for encoding.
	    $url = strtolower($url);
	    $url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator
	    
	    return $url;
	}
}
